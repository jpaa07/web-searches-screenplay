package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import screenplay.tasks.Search;
import screenplay.user_interface.GooglePage;

public class SearchSteps {

    private Actor googleActor = new Actor("Google User");
    private final WebDriver browser = new ChromeDriver();
    private GooglePage googlePage = new GooglePage();

    @Before
    public void assignBrowsersToActors() {
        googleActor.can(BrowseTheWeb.with(browser));
    }

    @Given("^I go to google$")
    public void go_to_google() {
        googleActor.attemptsTo(
                Open.browserOn(googlePage)
        );
    }

    @When("^I search the word \"(.*)\"$")
    public void search_in_google(String textToSearch){
        googleActor.attemptsTo(
                Search.inGoogle(textToSearch)
        );
    }

}
