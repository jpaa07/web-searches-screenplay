package screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("http://google.com")
public class GooglePage extends PageObject {
    public static Target SEARCH_BAR = Target.the("Google search bar").located(By.name("q"));
    public static Target SEARCH_BUTTON = Target.the("Google search button").located(By.name("btnK"));
}
