package screenplay.tasks;

import models.Searchers;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;
import screenplay.user_interface.GooglePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Search implements Task {

    private static Searchers searcher;
    private String textToSearch;

    public Search(String textToSearch) {
        this.textToSearch = textToSearch;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        switch (searcher) {
            case GOOGLE:
                actor.attemptsTo(
                        Enter.theValue(textToSearch).into(GooglePage.SEARCH_BAR),
                        Hit.the(Keys.ENTER).into(GooglePage.SEARCH_BAR));
                break;
            case YAHOO:
                break;
            case BING:
                break;
        }

    }

    public static Search inGoogle(String textToSearch) {
        searcher = Searchers.GOOGLE;
        return instrumented(Search.class, textToSearch);
    }
}
